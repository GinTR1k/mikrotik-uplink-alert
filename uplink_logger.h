#include <fstream>
#include <string>
#include <stdio.h>
#include <iostream>
using namespace std;


class UplinkLogger
{
    private:
        string log_file_name;

        string getTimestamp()
        {
            /* чтение файла, и удаление строки */
            fstream log_file;
            string timestamp_str;

            log_file.open(this->log_file_name);

            // Если файла не существует, возвращаем пустую строку
            if (log_file.fail())
            {
                return "";
            }

            getline(log_file, timestamp_str);
            log_file.close();

            return timestamp_str;
        }

        time_t parseTimestamp(string timestamp)
        {
            /* Парсинг строки в time_t */
            return (time_t)stoi(timestamp.c_str());
        }

    public:
        UplinkLogger(string log_file_name)
        {
            this->log_file_name = log_file_name;
        }

        void down(time_t timestamp)
        {
            /* Записываем в лог */
            ofstream log_file;

            log_file.open(this->log_file_name);
            log_file << timestamp << endl;
            log_file.close();
        }

        time_t up()
        {
            /* Считываем из лога */
            string timestamp = this->getTimestamp();

            // Времени может и не быть
            if (timestamp.empty())
            {
                return 0;
            }

            // Парсим строку и возвращаем время
            return this->parseTimestamp(timestamp);
        }

        void clear()
        {
            /* Удалить файл лога */
            remove(this->log_file_name.c_str());
        }
};
