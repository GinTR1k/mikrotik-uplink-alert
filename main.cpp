#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <curl/curl.h>

#include <uplink_logger.h>
using namespace std;


char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return find(begin, end, option) != end;
}

bool sendTelegramMessage(int down_time_in_seconds, string bandwidth)
{
    /* Отправка сообщения в Телеграм */
    CURL *curl;
    CURLcode res;

    // Форматирование текста сообщения
    string message_text = string(getenv("UPLINK_ALERT_URL_TELEGRAM")) +
        "Uplink+was+down+for+" + to_string(down_time_in_seconds) +
        "+seconds%21";

    if (!bandwidth.empty())
    {
        message_text.append("%0AThe+current+bandwidth+is+" + bandwidth);
    }

    curl = curl_easy_init();

    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, message_text.c_str());

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK)
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);

        return true;
    }

    return false;
}


int main(int argc, char * argv[])
{
    UplinkLogger uplink_logger = UplinkLogger("down.log");

    // Получаем текущее время
    time_t current_timestamp = time(nullptr);
    // Время последнего падения аплинка
    time_t last_down_timestamp;

    // Если есть аргумент --down, значит логируем отключение аплинка
    if (cmdOptionExists(argv, argv+argc, "--down"))
    {
        uplink_logger.down(current_timestamp);
    }
    // При аргументе --up отправляем сообщение в телегу
    else if (cmdOptionExists(argv, argv+argc, "--up"))
    {
        last_down_timestamp = uplink_logger.up();

        // Вытягиваем из аргументов максмимальную пропускную полосу
        string bandwidth;

        if (cmdOptionExists(argv, argc+argv, "--bandwidth"))
        {
            bandwidth = getCmdOption(argv, argc+argv, "--bandwidth");
        }

        // Если время последнего падения есть,
        // отправляем сообщение в телегу
        if (last_down_timestamp)
        {
            // Отправка сообщения в телегу
            // о количестве секунд, сколько пролежал аплинк
            bool is_message_sent = sendTelegramMessage(difftime(current_timestamp, last_down_timestamp), bandwidth);

            // Если сообщение было отправлено, удаляем лог
            if (is_message_sent == true)
            {
                uplink_logger.clear();
            }
        }
    }

    return 0;
}
